// playlist
// playlist-pos
// playlist-pos-1
// playlist-count
// playlist-start
// keep-open
// resume-playback
// save-position-on-quit
// watch-later-directoory
// shuffle
// quiet
// idle
// terminal
// msg-color
// length
// loadfile
// loadlist
// playlist-{clear,remove,move,shuffle}
// look into hooks, to run command on song change
// _, err = c.Write([]byte(`{ "command": ["set_property", "pause", true]  }` + "\n"))
// _, _ = c.Write([]byte(`{ "command": ["get_property", "path"]  }` + "\n"))
// _, _ = c.Write([]byte(`{ "command": ["get_property", "sdl-buflen"]  }` + "\n"))

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/jroimartin/gocui"
)

func main() {
	var b bytes.Buffer
	conn, err := net.Dial("unix", "/tmp/mpvsocket")
	if err != nil && os.Args[1] == "play" {
		c := exec.Command("mpv", "--no-terminal", "--audio-display=no", "--input-ipc-server=/tmp/mpvsocket", os.Args[2])
		c.Run()
	}
	if err != nil {
		fmt.Println("dial error:", err)
		return
	}
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "play":
			if len(os.Args) == 3 {
				conn.Write([]byte(`{ "command": ["stop"]  }` + "\n"))
				c := exec.Command("mpv", "--no-terminal", "--audio-display=no", "--input-ipc-server=/tmp/mpvsocket", os.Args[2])
				c.Run()
			}
		case "toggle":
			conn.Write([]byte(`{ "command": ["keypress", "p"]  }` + "\n"))
		case "stop":
			// add long songs to watch-later
			conn.Write([]byte(`{ "command": ["stop"]  }` + "\n"))
		case "next":
			// add long songs to watch-later
			conn.Write([]byte(`{ "command": ["keypress", ">"]  }` + "\n"))
		case "prev":
			conn.Write([]byte(`{ "command": ["keypress", "<"]  }` + "\n"))
		case "shuffle":
			conn.Write([]byte(`{ "command": ["playlist-shuffle"]  }` + "\n"))
		case "forward":
			conn.Write([]byte(`{ "command": ["keypress", "RIGHT"]  }` + "\n"))
		case "backward":
			conn.Write([]byte(`{ "command": ["keypress", "LEFT"]  }` + "\n"))
			// OUTPUT /////////////////////////////////////////////////////////////////
		case "fav":
			conn.Write([]byte(`{ "command": ["get_property", "path"]  }` + "\n"))
			data := copyStream(conn, b)
			d := string((unmarshalSong(data).Data).(string))
			f, err := ioutil.ReadFile(d)
			if err != nil {
				fmt.Println(err)
			}
			// TODO: Figure out why I can't write the file
			spl := strings.Split(d, "/")[len(strings.Split(d, "/"))-2:]
			new, _ := os.Create("~/Music/" + spl[0] + " - " + spl[1])
			_, err = new.Write(f)
			// err = ioutil.WriteFile("~/Music/"+spl[0]+" - "+spl[1], f, 0644)
			if err != nil {
				log.Println(err)
			}
		case "live":
			conn.Close()
			tc := time.NewTicker(500 * time.Millisecond)
			for _ = range tc.C {
				var percent bytes.Buffer
				var song bytes.Buffer
				conn, _ := net.Dial("unix", "/tmp/mpvsocket")
				conn.Write([]byte(`{ "command": ["get_property", "percent-pos"]  }` + "\n"))
				songStuff := copyStream(conn, percent)

				conn2, _ := net.Dial("unix", "/tmp/mpvsocket")
				conn2.Write([]byte(`{ "command": ["get_property", "filename"]  }` + "\n"))
				percentStuff := copyStream(conn2, song)
				song_ := string(unmarshalSong(percentStuff).Data.(string))

				conn3, _ := net.Dial("unix", "/tmp/mpvsocket")
				conn3.Write([]byte(`{ "command": ["get_property", "playlist"]  }` + "\n"))
				playlistStuff := copyStream(conn3, song)
				pl := unmarshalPlaylist(playlistStuff)

				fmt.Printf("\033[2J\n")
				boldRed := color.New(color.Bold, color.FgRed)

				for _, k := range pl.Data {
					spl := strings.Split(k["filename"], "/")
					final := spl[len(spl)-2] + " - " + spl[len(spl)-1]
					if spl[len(spl)-1] == song_ {
						boldRed.Println(final)
					} else {
						fmt.Println(final)
					}
				}

				bold := color.New(color.Bold)
				bold.Printf("%s%% - %s", strconv.FormatFloat(float64(unmarshalSong(songStuff).Data.(float64)), 'f', 6, 64), song_)
			}
		case "position":
			conn.Write([]byte(`{ "command": ["get_property", "percent-pos"]  }` + "\n"))
			data := copyStream(conn, b)
			fmt.Println(unmarshalSong(data).Data)
		case "current":
			conn.Write([]byte(`{ "command": ["get_property", "filename"]  }` + "\n"))
			data := copyStream(conn, b)
			fmt.Println(unmarshalSong(data).Data)
		case "pl":
			conn.Write([]byte(`{ "command": ["get_property", "playlist"]  }` + "\n"))
			data := copyStream(conn, b)
			pl := unmarshalPlaylist(data)
			for _, k := range pl.Data {
				fmt.Println(k["filename"])
			}
		case "tui":
			g, _ := gocui.NewGui(gocui.OutputNormal)
			defer g.Close()
			g.SetManagerFunc(layout)
			if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
				log.Panicln(err)
			}
			if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
				log.Panicln(err)
			}
		}
	}

}

func layout(g *gocui.Gui) error {
	var b bytes.Buffer
	conn, _ := net.Dial("unix", "/tmp/mpvsocket")

	maxX, maxY := g.Size()
	if v, err := g.SetView("playlist", 0, 0, maxX-1, maxY-1); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "MPV(r) Playlist Manager"
		conn.Write([]byte(`{ "command": ["get_property", "playlist"]  }` + "\n"))
		data := copyStream(conn, b)
		pl := unmarshalPlaylist(data)
		for _, k := range pl.Data {
			s := strings.SplitAfter(k["filename"], "/")
			fmt.Fprintln(v, s[len(s)-3]+s[len(s)-2]+s[len(s)-1])
		}

	}
	return nil
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

type songData struct {
	Data  interface{} `json:"data"`
	Error string      `json:"error"`
}

type playlistData struct {
	Data  []map[string]string `json:"data"`
	Error string              `json:"error"`
}

func unmarshalPlaylist(b bytes.Buffer) playlistData {
	objs := strings.Split(b.String(), "\n")
	pl := playlistData{}
	json.Unmarshal([]byte(objs[0]), &pl)
	return pl
}

func unmarshalSong(b bytes.Buffer) songData {
	objs := strings.Split(b.String(), "\n")
	song := songData{}
	json.Unmarshal([]byte(objs[0]), &song)
	return song
}

func copyStream(c net.Conn, b bytes.Buffer) bytes.Buffer {
	go func(c net.Conn) {
		time.Sleep(10 * time.Millisecond)
		c.Close()
	}(c)

	// Always returns error
	io.Copy(&b, c)
	return b
}
